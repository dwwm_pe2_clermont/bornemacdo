export class Produit {
    id: number;
    nom: string;
    image: string;
    prix: number;


  constructor(id: number,nom: string, image: string, prix: number) {
    this.id = id;
    this.nom = nom;
    this.image = image;
    this.prix = prix;
  }
}
