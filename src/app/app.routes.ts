import { Routes } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {LoginComponent} from "./components/login/login.component";
import {CommandDetailComponent} from "./components/command-detail/command-detail.component";

export const routes: Routes = [
  {
    path: '', component: HomeComponent,
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'command/:id', component: CommandDetailComponent
  }
];
