import { HttpInterceptorFn } from '@angular/common/http';
import {inject} from "@angular/core";
import {AuthService} from "../services/auth.service";

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  let authService = inject(AuthService);
  let cloneReq = req.clone();
  if(authService.isConnected){
    cloneReq = req.clone({
      headers: req.headers.set('Authorization', "Bearer "+window.localStorage.getItem("token"))
    });
  }
  return next(cloneReq);
};
