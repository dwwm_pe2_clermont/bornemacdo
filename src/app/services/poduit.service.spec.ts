import { TestBed } from '@angular/core/testing';

import { PoduitService } from './poduit.service';

describe('PoduitService', () => {
  let service: PoduitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PoduitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
