import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {User} from "../models/user";
import {catchError, Observable, retry, throwError} from "rxjs";
import {environment} from "../../environments/environment.development";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isConnected = false;
  urlApi = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
    // On vérifie si on a un token
    if(<string>window.localStorage.getItem("token")){
      // On vérifie qu'il n'est pas expiré
      if(!this.tokenExpired(<string>window.localStorage.getItem("token"))){
        this.isConnected = true;
      }
    }
  }

  authenticate(user: User): Observable<{token: string}>{
    return this.httpClient.post<{token: string}>(this.urlApi+"auth", user).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new ErrorEvent(error.error.message));
  }

  private tokenExpired(token: string) {
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime() / 1000)) >= expiry;
  }

}
