import { Injectable } from '@angular/core';
import {Produit} from "../models/produit";
import {Command} from "../models/command";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../environments/environment.development";
import {catchError, Observable, retry, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommandService {

  produits: Produit[] = [];
  urlApi = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
    if(<string>window.localStorage.getItem("command")){
      this.produits = JSON.parse(<string>window.localStorage.getItem("command"));
    } else {
      this.produits = [];
    }
  }

  sendCommand(): Observable<{ id: number }>{
      let command = new Command();

      this.produits.forEach(elem => {
        command.products.push('/api/products/'+elem.id)
      });

      return this.httpClient.post<{ id: number }>(this.urlApi+'commands', command).pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new ErrorEvent('Something bad happened; please try again later.'));
  }

  addProduit(produit: Produit): Produit[] {
    this.produits.push(produit);
    window.localStorage.setItem("command", JSON.stringify(this.produits));
    return this.produits;
  }

  removeProduit(produits: Produit[]): Produit[] {
    this.produits =  produits;
    window.localStorage.setItem("command", JSON.stringify(this.produits));
    return this.produits;
  }
}
