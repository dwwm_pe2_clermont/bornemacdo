import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Produit} from "../../models/produit";
import {CommonModule} from "@angular/common";
import {ProductComponent} from "../product/product.component";
import {PoduitService} from "../../services/poduit.service";
import {environment} from "../../../environments/environment.development";

@Component({
  selector: 'app-carte',
  standalone: true,
  imports: [CommonModule, ProductComponent],
  templateUrl: './carte.component.html',
  styleUrl: './carte.component.css'
})
export class CarteComponent implements OnInit {

  products ?: Produit[];
  isLoading: boolean = true;

  @Output() commendCarteEmitter: EventEmitter<Produit> = new EventEmitter<Produit>();

  constructor(private produitService: PoduitService) {
  }
  carteCommandEmitter($event: Produit) {
    console.log($event);
    this.commendCarteEmitter.emit($event)
  }

  ngOnInit(): void {
    this.produitService.getAll().subscribe(data => {
      this.products = data;
      this.isLoading = false;
    });
  }
}
