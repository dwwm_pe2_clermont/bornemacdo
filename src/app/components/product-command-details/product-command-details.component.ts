import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Produit} from "../../models/produit";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {environment} from "../../../environments/environment.development";

@Component({
  selector: 'app-product-command-details',
  standalone: true,
  imports: [MatButtonModule, MatIconModule],
  templateUrl: './product-command-details.component.html',
  styleUrl: './product-command-details.component.css'
})
export class ProductCommandDetailsComponent {
  imageUrl = environment.imageUrl;
  @Input() produit?: Produit;
  @Output() deleteProductEventEmitter: EventEmitter<Produit> = new EventEmitter<Produit>();

  deleteProduct() {
    this.deleteProductEventEmitter.emit(this.produit);
  }
}
