import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCommandDetailsComponent } from './product-command-details.component';

describe('ProductCommandDetailsComponent', () => {
  let component: ProductCommandDetailsComponent;
  let fixture: ComponentFixture<ProductCommandDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ProductCommandDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProductCommandDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
