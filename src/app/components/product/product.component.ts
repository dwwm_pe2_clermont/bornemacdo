import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Produit} from "../../models/produit";
import {environment} from "../../../environments/environment.development";

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [],
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {
  @Input() product?: Produit;
  @Output() productCommandEmitter: EventEmitter<Produit> = new EventEmitter<Produit>();
  imageUrl: string = environment.imageUrl;

  commandProduct() {
    this.productCommandEmitter.emit(this.product);
  }
}
