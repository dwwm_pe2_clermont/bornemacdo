import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Produit} from "../../models/produit";
import {ProductCommandDetailsComponent} from "../product-command-details/product-command-details.component";
import {CommonModule} from "@angular/common";
import {AuthService} from "../../services/auth.service";
import {Router, RouterModule} from "@angular/router";
import {CommandService} from "../../services/command.service";

@Component({
  selector: 'app-commande',
  standalone: true,
  imports: [ProductCommandDetailsComponent, CommonModule, RouterModule],
  templateUrl: './commande.component.html',
  styleUrl: './commande.component.css'
})
export class CommandeComponent {
  @Input() commande: Produit[] = [];
  @Output() suppressionCommandEventEmitter: EventEmitter<Produit> = new EventEmitter<Produit>();
  isConnected = this.authService.isConnected;

  constructor(private authService: AuthService, private commandService: CommandService,
              private router: Router) {
  }

  deleteProduct($event: Produit) {
    this.suppressionCommandEventEmitter.emit($event)
  }

  command() {
    this.commandService.sendCommand().subscribe(data => {
      this.router.navigate(["/command", data.id]);
    });
  }
}


