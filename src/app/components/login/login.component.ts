import { Component } from '@angular/core';
import {User} from "../../models/user";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  loader = false;
  user = new User();
  strError = '';
  constructor(private authService: AuthService, private router: Router) {
  }

  login(){
    this.loader = true;
    this.authService.authenticate(this.user).subscribe(data => {
      window.localStorage.setItem("token", data.token);
      this.router.navigate(["/"]);
      this.authService.isConnected = true;
    }, error => {
      this.strError = error.type;
      this.loader = false;
    } )
  }
}
