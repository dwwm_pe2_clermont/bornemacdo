import { Component } from '@angular/core';
import {Produit} from "../../models/produit";
import {CommandService} from "../../services/command.service";
import {RouterModule, RouterOutlet} from "@angular/router";
import {HeaderComponent} from "../header/header.component";
import {CarteComponent} from "../carte/carte.component";
import {CommandeComponent} from "../commande/commande.component";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [RouterModule, HeaderComponent, CarteComponent, CommandeComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  command: Produit[] = this.commandServic.produits;

  constructor(private commandServic: CommandService) {
  }

  commandReceived($event: Produit) {
    this.command = this.commandServic.addProduit($event);
  }

  deleteProduct($event: Produit) {
    // Je cré un nouveau tableau de produit
    let newCommand: Produit[] = [];
    // Je déclare un booléen pour savoir si l'élément à été supprime
    let remove = false;
    // Je parcours toutes mes commandes
    for(let i = 0; i<this.command.length; i++){
      // Si le burger est le burger à supprimé ou si j'ai déjà supprimé un burger
      if(this.command[i] !== $event || remove){
        // J'ajoute le burger dans ma liste de commande
        newCommand.push(this.command[i]);
      } else {
        // Je n'ajoute pas de burger (il sera donc supprimé)
        remove = true;
      }
    }

    this.command = this.commandServic.removeProduit(newCommand);
  }
}
